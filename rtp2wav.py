import os
import os.path

import yaml
import argparse
import pyshark
import pywav
import time

import pydub
from pydub import AudioSegment

from threading import Thread
from queue import Queue


def loadFilter(configFile):
    fileFilter = open(configFile, 'r')
    try:
        filter = yaml.safe_load(fileFilter)
    except yaml.YAMLError as err:
        print("ERROR : Load Filter File")
    return filter


def loadPCAP(pcapFile: str, filter):
    capturedData = pyshark.FileCapture(
        pcapFile, display_filter=filter, debug=True)
    return capturedData


def decideCodecType(rtpPacket: pyshark.packet.layer.Layer) -> str:
    codecTypes = {"0": "PCMU", "3": "GSM", "8": "PCMA", "9": "G722"}
    codec = codecTypes[rtpPacket.p_type]
    return codec


def decideCodecType4PyWav(codec: str) -> int:
    pywavCodecTypes = {"PCMU": 7, "PCMA": 6, "PCM": 1}
    return pywavCodecTypes[codec]


def packetPayload2RawAudio(payload: str) -> bytearray:
    return bytearray.fromhex(payload)


def raw2wav(
    audio: bytearray, fn: str, c: int = 1, br: int = 8000, bps: int = 8, fmt: int = 8
):
    wave_write = pywav.WavWrite(fn, c, br, bps, fmt)
    wave_write.write(audio)
    wave_write.close()


def collectingPayloadBySession(rtpPacket: pyshark.packet.layer.Layer, container):
    if container.get(rtpPacket.ssrc, None) == None:
        container[rtpPacket.ssrc] = []
    if rtpPacket.payload:
        container[rtpPacket.ssrc].append(rtpPacket.payload)
    return container


def collectingCodecBySession(rtpPacket: pyshark.packet.layer.Layer, container):
    if container.get(rtpPacket.ssrc, None) == None:
        container[rtpPacket.ssrc] = []
    if rtpPacket.p_type:
        container[rtpPacket.ssrc] = decideCodecType(rtpPacket)
    return container


def collectingPairSession(packet: pyshark.packet.packet.Packet, container):
    ipFrame = packet[1]
    udpFrame = packet[2]
    rtpFrame = packet[3]

    src = ":".join([ipFrame.src, udpFrame.port])
    dst = ":".join([ipFrame.dst, udpFrame.dstport])

    # Check if already have pair
    if src in container:
        if container[src].get("dst_ssrc", None) != None:
            return container

    # Check pair first
    if dst in container:
        if container[dst]["dst"] == src:
            container[dst]["dst_ssrc"] = rtpFrame.ssrc
            return container

    if container.get(src, None) == None:
        container[src] = {}
    if rtpFrame.payload:
        container[src] = {"dst": dst,
                          "src_ssrc": rtpFrame.ssrc, "dst_ssrc": None}
    return container


def readRTPPackets(
    capturedData: pyshark.capture.file_capture.FileCapture,
    listPair: dict = {},
    listRTPCodec: dict = {},
    listRTP: dict = {},
):
    for packet in capturedData:
        rtpPacket = packet[3]
        listPair = collectingPairSession(packet, listPair)
        listRTPCodec = collectingCodecBySession(rtpPacket, listRTPCodec)
        listRTP = collectingPayloadBySession(rtpPacket, listRTP)

    return listPair, listRTPCodec, listRTP


def concatPayload(packet: list) -> str:
    return " ".join(packet).replace(":", " ")


def audioSeparation(session, rtp_list, rtp_codec_list, outdir=""):
    rtp_packet = rtp_list[session]
    codec = rtp_codec_list[session]

    fmt = decideCodecType4PyWav(codec)
    payload = concatPayload(rtp_packet)
    audio = packetPayload2RawAudio(payload)
    output = os.path.join(outdir, f"{session}.wav")

    raw2wav(audio, fn=output, fmt=fmt)


def converterThread(queue, outdir, rtp_codec_list, rtp_list, pair_list):
    while True:
        rtp_ssrc = queue.get()

        audioSeparation(rtp_ssrc, rtp_list, rtp_codec_list, outdir=outdir)

        queue.task_done()


def waitingFileExist(path):
    # waiting for file exist
    while not os.path.exists(path):
        time.sleep(1)


def openPairAudio(pair_list, outdir):
    au1 = pair_list.get("src_ssrc")
    au2 = pair_list.get("dst_ssrc", None)
    if au2 == None:
        return False

    au1_path = os.path.join(outdir, f"{au1}.wav")
    au2_path = os.path.join(outdir, f"{au2}.wav")

    waitingFileExist(au1_path)
    first = AudioSegment.from_file(au1_path, format="wav")

    waitingFileExist(au2_path)
    second = AudioSegment.from_file(au2_path, format="wav")
    fn = "-".join([au1, f"{au2}.wav"])
    return first, second, fn


def combinePair(
    first: pydub.audio_segment.AudioSegment,
    second: pydub.audio_segment.AudioSegment,
    fn: str,
    position=0,
):
    combined = first.overlay(second, position)
    combined.export(fn, format="wav")


def mergingThread(queue, outdir, pair_list):
    while True:
        pair = queue.get()

        audios = openPairAudio(pair_list[pair], outdir)

        outdir = outdir + "/merge"
        os.mkdir(outdir)

        if audios:
            first, second, fn = audios
            combinePair(first, second, os.path.join(outdir, fn))
        queue.task_done()


def main():
    # DONE: Parsing argument
    parser = argparse.ArgumentParser(description="RTP-to-Audio Converter")
    parser.add_argument(
        "-i", "--input", required=True, help="Input file (.pcap) or Available Network Interface (i.e: eth1 *For Linux)")
    parser.add_argument(
        "-o", "--output", required=True, help="path/to/output directory for parsed audio")
    parser.add_argument(
        "-c", "--config", required=False, help="path/to/config.yml path to rtp filter"
    )
    args = parser.parse_args()
    print("Input: "+str(args.input))
    print("Output directory: "+str(args.output))
    print("Filter: "+str(args.config))
    pcapFile = ''

    # DONE: Check Input File (.PCAP)
    if os.path.exists(args.input):
        pcapFile = args.input
    else:
        print("ERROR: PCAP File not found!")
    print(pcapFile)

    # DONE: Check Output Directory
    if os.path.exists(args.output):
        outDir = args.output
    else:
        outDir = os.mkdir(args.output)
    print(outDir)

    # DONE: Load Config and Filter
    filterFile = loadFilter(args.config)
    if(filterFile):
        filter = filterFile["filter"]
    else:
        filter = "rtp"
    print(filter)

    # DONE: Capturing Process
    capturedData = loadPCAP(pcapFile, filter)
    listPair, listRTPCodec, listRTP = readRTPPackets(capturedData)

    queueConvertContainer = Queue()
    queueMergeContainer = Queue()

    threads = []
    for i in range(int(os.cpu_count()*0.5)):
        t1 = Thread(
            target=converterThread,
            args=(queueConvertContainer, outDir,
                  listRTPCodec, listRTP, listPair),
            daemon=True,
        )
        t2 = Thread(
            target=mergingThread,
            args=(
                queueMergeContainer, outDir, listPair), daemon=True
        )
        threads.append(t1)
        threads.append(t2)
        t1.start()
        t2.start()

    # DONE: Converting RAW audio to WAV
    for rtp_ssrc in listRTP:
        queueConvertContainer.put(rtp_ssrc)

    # DONE: Merging Pair WAV into Single WAV
    for pair in listPair:
        queueMergeContainer.put(pair)

     # block until all tasks are done
    queueConvertContainer.join()
    queueMergeContainer.join()

print("Finish")

if __name__ == "__main__":
    main()
