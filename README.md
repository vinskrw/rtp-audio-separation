
<!-- PROJECT LOGO -->
<br />
<p align="center">
  <h3 align="center">rtp-audio-separation</h3>
</p>

<!-- TABLE OF CONTENTS -->
## Table of Contents

* [Getting Started](#getting-started)
    * [Usage](#usage)
    * [Configuration Files](#configuration-files)
* [License](#license)
* [Contact](#contact)

## Getting Started

<p align="center">
  This is program that reads raw RTP packet data from a file or network interface and produces audio files.
</p>

### Usage
```bash
python rtp2wav.py -i test_180s.pcap -o ./output -c config.yml
```

### Configuration Sample
```yaml
# config.yml
filter: rtp.ssrc==0x46873251||rtp.ssrc==0x0d730ec4
```
